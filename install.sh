#!/bin/bash

## init the controller node's env, should be run in root mode
# started from a server only install OpenSSH server

## init the env
source $(dirname "$0")/env.sh


# upgrade the system to the newest version
apt-get update
apt-get upgrade -y


# install bridge-utils, the OpenStack network needs this
apt-get install -y bridge-utils


# install ntp, make sure that the time on the server stays in sync with an external server. If the Internet connectivity is down, the NTP server uses its own hardware clock as the fallback 
apt-get install -y ntp
sed -i 's/server ntp.ubuntu.com/server ntp.ubuntu.com\nserver 127.127.1.0\nfudge 127.127.1.0 stratum 10/g' /etc/ntp.conf
service ntp restart


# install mysql, need set the master password
export DEBIAN_FRONTEND=noninteractive
apt-get install -y -q python-mysqldb mysql-server
mysql -uroot -e <<EOSQL "UPDATE mysql.user SET Password=PASSWORD('$MYSQL_ROOT_PASSWORD') WHERE User='root'; FLUSH PRIVILEGES;"
EOSQL
sed -i 's/bind-address		= 127.0.0.1/bind-address		= 0.0.0.0/g' /etc/mysql/my.cnf
service mysql restart

# create all the needed dabase in the mysql server for keystone/glance/nova
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE $MYSQL_KEYSTONE_DBNAME;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE USER $MYSQL_KEYSTONE_ADMIN_USER;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_KEYSTONE_DBNAME.* TO '$MYSQL_KEYSTONE_ADMIN_USER'@'%' IDENTIFIED BY '$MYSQL_KEYSTONE_ADMIN_PASSWORD';"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_KEYSTONE_DBNAME.* TO '$MYSQL_KEYSTONE_ADMIN_USER'@localhost IDENTIFIED BY '$MYSQL_KEYSTONE_ADMIN_PASSWORD';"
#mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "SET PASSWORD FOR '$MYSQL_KEYSTONE_ADMIN_USER'@'%' = PASSWORD('$MYSQL_KEYSTONE_ADMIN_PASSWORD');"

mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE $MYSQL_GLANCE_DBNAME;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE USER $MYSQL_GLANCE_ADMIN_USER;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_GLANCE_DBNAME.* TO '$MYSQL_GLANCE_ADMIN_USER'@'%' IDENTIFIED BY '$MYSQL_GLANCE_ADMIN_PASSWORD';"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_GLANCE_DBNAME.* TO '$MYSQL_GLANCE_ADMIN_USER'@localhost IDENTIFIED BY '$MYSQL_GLANCE_ADMIN_PASSWORD';"
#mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "SET PASSWORD FOR '$MYSQL_GLANCE_ADMIN_USER'@'%' = PASSWORD('$MYSQL_GLANCE_ADMIN_PASSWORD');"

mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE $MYSQL_NOVA_DBNAME;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE USER $MYSQL_NOVA_ADMIN_USER;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_NOVA_DBNAME.* TO '$MYSQL_NOVA_ADMIN_USER'@'%' IDENTIFIED BY '$MYSQL_NOVA_ADMIN_PASSWORD';"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_NOVA_DBNAME.* TO '$MYSQL_NOVA_ADMIN_USER'@localhost IDENTIFIED BY '$MYSQL_NOVA_ADMIN_PASSWORD';"
#mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "SET PASSWORD FOR '$MYSQL_NOVA_ADMIN_USER'@'%' = PASSWORD('$MYSQL_NOVA_ADMIN_PASSWORD');"

mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE $MYSQL_HORIZON_DBNAME;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "CREATE USER $MYSQL_HORIZON_ADMIN_USER;"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_HORIZON_DBNAME.* TO '$MYSQL_HORIZON_ADMIN_USER'@'%' IDENTIFIED BY '$MYSQL_HORIZON_ADMIN_PASSWORD';"
mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "GRANT ALL PRIVILEGES ON $MYSQL_HORIZON_DBNAME.* TO '$MYSQL_HORIZON_ADMIN_USER'@localhost IDENTIFIED BY '$MYSQL_HORIZON_ADMIN_PASSWORD';"
#mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "SET PASSWORD FOR '$MYSQL_NOVA_ADMIN_USER'@'%' = PASSWORD('$MYSQL_NOVA_ADMIN_PASSWORD');"


## install OpenStack
# install keystone
apt-get install -y keystone python-keystone python-keystoneclient
sed -i "s/admin_token = ADMIN/admin_token = $SERVICE_TOKEN/g" /etc/keystone/keystone.conf
sed -i "s/connection = sqlite:\/\/\/\/var\/lib\/keystone\/keystone.db/connection = mysql:\/\/$MYSQL_KEYSTONE_ADMIN_USER:$MYSQL_KEYSTONE_ADMIN_PASSWORD@$SERVICE_IP\/keystone/g" /etc/keystone/keystone.conf

service keystone restart
keystone-manage db_sync

source $(dirname "$0")/keystone_data.sh
source $(dirname "$0")/keystone_endpoints.sh

# install glance
apt-get install -y glance glance-api glance-client glance-common glance-registry python-glance

# glance-api-paste.ini
sed -i "s/admin_tenant_name = %SERVICE_TENANT_NAME%/admin_tenant_name = $TENANT_SERVICE_NAME/g" /etc/glance/glance-api-paste.ini
sed -i "s/admin_user = %SERVICE_USER%/admin_user = $KEYSTONE_GLANCE_USER_NAME/g" /etc/glance/glance-api-paste.ini
sed -i "s/admin_password = %SERVICE_PASSWORD%/admin_password = $KEYSTONE_GLANCE_USER_PASSWORD/g" /etc/glance/glance-api-paste.ini

# glance-registry-paste.ini
sed -i "s/admin_tenant_name = %SERVICE_TENANT_NAME%/admin_tenant_name = $TENANT_SERVICE_NAME/g" /etc/glance/glance-registry-paste.ini
sed -i "s/admin_user = %SERVICE_USER%/admin_user = $KEYSTONE_GLANCE_USER_NAME/g" /etc/glance/glance-registry-paste.ini
sed -i "s/admin_password = %SERVICE_PASSWORD%/admin_password = $KEYSTONE_GLANCE_USER_PASSWORD/g" /etc/glance/glance-registry-paste.ini

# glance-registry.conf
sed -i "s/sql_connection = sqlite:\/\/\/\/var\/lib\/glance\/glance.sqlite/sql_connection = mysql:\/\/$MYSQL_GLANCE_ADMIN_USER:$MYSQL_GLANCE_ADMIN_PASSWORD@$SERVICE_IP\/glance/g" /etc/glance/glance-registry.conf 

echo "" >> /etc/glance/glance-registry.conf
echo "[paste_deploy]" >> /etc/glance/glance-registry.conf
echo "flavor = keystone" >> /etc/glance/glance-registry.conf

echo "" >> /etc/glance/glance-api.conf
echo "[paste_deploy]" >> /etc/glance/glance-api.conf
echo "flavor = keystone" >> /etc/glance/glance-api.conf

glance-manage version_control 0
glance-manage db_sync

service glance-api restart
service glance-registry restart


